use tokio::{net::TcpListener};
use tokio::io::{Result, Error};
use std::net::{IpAddr, Ipv4Addr};
use std::sync::{Arc, Mutex};

use crate::input;

const IP: IpAddr = IpAddr::V4(Ipv4Addr::new(0, 0, 0 ,0));
pub const PORT: u16 = 8081;

pub async fn run(input: input::Handle) -> Result<()> {
    let current_id = Arc::new(Mutex::new(0_u64));
    let listener = TcpListener::bind((IP, PORT)).await?;
    log::info!("backend is listening on: {}:{}", IP, PORT);

    loop {
        let current_id = current_id.clone();
        let (socket, _addr) = listener.accept().await?;
        let socket = socket.into_std().unwrap();
        socket.set_nonblocking(false).unwrap();

        let input = input.clone();
        tokio::task::spawn_blocking(move || {
            match tungstenite::accept(socket) {
                Ok(mut websocket) => {
                    // id handling
                    let mut current_id = current_id.lock().unwrap();
                    let id = *current_id;
                    log::info!("id: {}", id);
                    *current_id += 1;
                    drop(current_id);

                    loop {
                        if let Ok(msg) = websocket.read_message() {
                            let locked_by_other: bool = match input.locked_by() {
                                Some(locked_id) => {
                                    id != locked_id 
                                }
                                None => false
                            };
                            match msg {
                                tungstenite::Message::Text(text) => {
                                    if !text.is_empty() {
                                        match text.chars().nth(0).unwrap_or('\0') {
                                            's' => {
                                                if !locked_by_other {
                                                    let split: Vec<&str> = text[1..].split("|").collect();
                                                    if split.len() == 2 {
                                                        if let Ok(x) = split[0].parse::<i8>() {
                                                            if let Ok(y) = split[1].parse::<i8>() {
                                                                input.set_xy([x, y]);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            'g' => {
                                                let xy = input.get_xy();
                                                let msg = format!("{}|{}|{}", xy[0], xy[1], if locked_by_other {1} else {0});
                                                websocket.write_message(tungstenite::Message::Text(msg)).unwrap();
                                            }
                                            'l' => {
                                                let _ = input.lock(id);
                                            }
                                            'r' => {
                                                input.release(id);
                                            }
                                            _ => ()
                                        }
                                    }
                                },
                                _ => (),
                            }
                        }
                    }
                },
                Err(e) => {
                    log::warn!("websocket error: {}", e);
                }
            }

            Ok::<(), Error>(())
        });
    }
}