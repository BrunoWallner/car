pub mod html;
pub mod js;

pub(super) fn slice(html: &String, start: usize, end: Option<usize>) -> String {
    let out = if start <= html.len() {
        if let Some(end) = end {
            if start <= end && end <= html.len() {
                String::from(&html[start..end])
            } else {
                String::new()
            }
        } else {
            String::from(&html[start..])
        }
    } else {
        String::new()
    };

    out
}