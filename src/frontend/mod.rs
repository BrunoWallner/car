mod processor;

use tokio::{net::TcpListener};
use tokio::io::{Result, AsyncWriteExt, Error};
use std::net::{IpAddr, Ipv4Addr};
use local_ip_address::local_ip;
use processor::{js, html, html::*};
use std::time::Instant;

const COMPACT_HTML: bool = true;
const INDEX: &str = "./frontend/";

const IP: IpAddr = IpAddr::V4(Ipv4Addr::new(0, 0, 0 ,0));
const PORT: u16 = 8080;

use crate::backend::PORT as BACKEND_PORT;

pub async fn run() -> Result<()> {
    let html = get_file(&(INDEX.to_owned() + "index.html")).unwrap();

    let compile_start = Instant::now();

    // include scripts
    let mut elements = html::parse(html.clone());
    for element in elements.iter_mut() {
        let mut deleted_attributes: Vec<usize> = Vec::new();
        for (i, attribute) in element.attributes.iter().enumerate() {
            // get start and end of 'SRC' attribute
            match attribute.kind {
                AttributeKind::Src => {
                    // paste in src content
                    let path = &(INDEX.to_owned() + &attribute.value.replace("\"", ""));
                    let content = get_file(path).expect("load src file content");
                    element.inner = Some(content);
                    deleted_attributes.push(i);
                },
                _ => (),
            }
        }
        for (i, d) in deleted_attributes.iter().enumerate() {
            element.attributes.remove(d - i);
        }
    }

    // javascript manipulation
    for element in elements.iter_mut() {
        match element.kind {
            Kind::Script => {
                if let Some(inner) = element.inner.as_mut() {
                    // include js method to connect to websocket
                    let local_ip = local_ip().unwrap_or(IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0)));
                    *inner = inner.replace("/* %Websocket% */", &format!("let socket = new WebSocket('ws://{}:{}');", local_ip, BACKEND_PORT));

                    *inner = js::remove_comments(inner.clone());
                }
            }
            Kind::Style => {

            }
            _ => (),
        }
    }

    let html = html::compile(elements, COMPACT_HTML);
    log::info!("sucessfully compiled html in {:#?}", compile_start.elapsed());

    let listener = TcpListener::bind((IP, PORT)).await?;
    log::info!("frontend is listening on: {}:{}", IP, PORT);

    loop {
        let (mut socket, _addr) = listener.accept().await?;
        let html = html.clone();
        tokio::spawn(async move {
            let status_line = "HTTP/1.1 200 OK";
            let response = format!(
                "{}\r\nContent-Length: {}\r\n\r\n{}",
                status_line,
                html.len(),
                html
            );

            socket.write_all(response.as_bytes()).await?;
            Ok::<(), Error>(())
        });
    }
}

use std::io::{Read, Result as IoResult};
use std::fs::File;

fn get_file(path: &str) -> IoResult<String> {
    let mut file = File::open(path)?;
    let mut buffer = String::new();
    file.read_to_string(&mut buffer)?;
    Ok(buffer)
}
