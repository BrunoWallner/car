// https://github.com/jmsv/l293d/blob/master/l293d/driver.py

use gpio_cdev::{Chip, LineHandle, Error, LineRequestFlags};
use std::thread::sleep;
use std::time::Duration;

/*
motor pins on board:
M1: [1, 2, 7],
M2: [9, 10, 15]
=> offset of 8 pins
*/

/*
pins on rpi
11 => 17
13 => 27
15 => 22
*/

pub struct Driver {
    motor_on: LineHandle,
    motor_ac: LineHandle,
    motor_c: LineHandle,
}
impl Driver {
    pub fn new() -> Result<Self, Error> {
        let mut chip = Chip::new("/dev/gpiochip0")?;
        let motor_on = chip
            .get_line(17)?
            .request(LineRequestFlags::OUTPUT, 0, "read-input")?;

        let motor_ac = chip
            .get_line(27)?
            .request(LineRequestFlags::OUTPUT, 0, "read-input")?;
        

        let motor_c = chip
            .get_line(22)?
            .request(LineRequestFlags::OUTPUT, 0, "read-input")?;

        Ok(Self {
            motor_on,
            motor_ac,
            motor_c,
        })
    }

    pub fn power(&self) -> Result<(), Error> {
        self.motor_c.set_value(255)?;
        for _ in 0..25500 {
            self.motor_on.set_value(255)?;
            sleep(Duration::from_micros(100));
            self.motor_on.set_value(255)?;
            sleep(Duration::from_micros(100));
        }
        Ok(())
    }
}