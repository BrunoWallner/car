const ID = "input";
const REFRESH_RATE = 20;

// global vars
let x = 0;
let y = 0;

let should_sync = false;
let input_sent = false;
let locked_by_other = false

function InitInput() {
    let joy = new JoyStick(ID,{
        // The ID of canvas element
        title: 'joystick',
        // width/height
        width: undefined,
        height: undefined,
        // Internal color of Stick
        internalFillColor: '#FFFFFF',
        // Border width of Stick
        internalLineWidth: 2,
        // Border color of Stick
        internalStrokeColor: '#FFFFFF',
        // External reference circonference width
        externalLineWidth: 2,
        //External reference circonference color
        externalStrokeColor: '#FFFFFF',
        // Sets the behavior of the stick
        autoReturnToCenter: false
    });
    
    /* will be compiled to "let socket = ..."" */
    /* %Websocket% */
    socket.onopen = function() {
        console.log("init setter");
        InitSetter(joy, socket);
        console.log("init getter");
        InitGetter(joy, socket);
    }
}

function InitSetter(joy, socket) {    
    // setter
    setInterval(sendInput, REFRESH_RATE);

    let joy_element = document.getElementById(ID);

    joy_element.addEventListener("mousedown", lock);
    joy_element.addEventListener("touchstart", lock);

    // must be global
    document.addEventListener("mouseup", release);
    document.addEventListener("touchend", release);
    
    function sendInput() {
        if (should_sync && !locked_by_other) {
            let new_x = Math.round(joy.GetX());
            let new_y = Math.round(joy.GetY());
    
            if (new_x != x || new_y != y) {
                x = new_x;
                y = new_y;
    
                socket.send("s" + x + "|" + y);
            }
        }
    }

    // lock / release
    let locked = false;
    function lock() {
        if (!locked) {
            locked = true;
            socket.send("l");
        }
    }

    function release() {
        if (locked) {
            locked = false;
            socket.send("r");
        }
    }
}

function InitGetter(joy, socket) {
    setInterval(function() {requestInput()}, REFRESH_RATE);
    requestInput();
    
    socket.onmessage = function(e) {
        let split = e.data.split("|");
        if (split.length == 3) {
            x = split[0];
            y = split[1];
            if (split[2] === "1") {
                locked_by_other = true;
                joy.SetBlocked(true);
                joy.SetInternalFillColor('#ff0000');
            } else {
                locked_by_other = false;
                joy.SetBlocked(false);
                joy.SetInternalFillColor('#FFFFFF');
            }
    
            joy.SetX(x);
            joy.SetY(y);
            joy.Redraw();

            should_sync = true;
        } 
    }
    
    function requestInput() {
        // console.log("locked: ", locked_by_other);
        socket.send("g");
    }
}
