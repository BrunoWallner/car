use super::*;

pub fn compile(elements: Vec<Element>, compact: bool) -> String {
    let mut html = String::new();

    for element in elements {
        let mut attributes: String = String::new();
        element
            .attributes
            .iter()
            .for_each(|f| {
                let add = format!(
                    "{}={}",
                    f.kind.as_string(),
                    f.value,
                );
                attributes.push_str(&format!(" {}", add));
            });
        
        let mut inner = compile(element.elements, compact);

        if let Some(i) = element.inner {
            inner.push_str(&i);
        }

        let add = format!(
            "<{}{}>{}{}{}",
            element.kind.as_string(),
            attributes,
            if compact {""} else {"\n"},
            inner,
            if element.kind.has_closing() {
                format!("</{}>{}", element.kind.as_string(), if compact {""} else {"\n"},)
            } else {
                format!("")
            },
        );
        html.push_str(&add);
    }

    html
}