mod frontend;
mod backend;
mod logger;
mod motor;
mod input;

use tokio::runtime;
use std::thread;


fn main() {
    let rt = runtime::Builder::new_multi_thread()
        .enable_io()
        .build()
        .unwrap();

    let _logger = logger::setup().unwrap();

    let input = input::Handle::init();
    // let driver = motor::Driver::new().unwrap();
    // driver.power().unwrap();

    rt.spawn(async move {
        frontend::run().await
    });
    
    let backend_input = input.clone();
    rt.spawn(async move {
        backend::run(backend_input).await.unwrap();
    });

    thread::park();
}