use std::sync::{Arc, Mutex};
use std::time::Duration;
use std::thread;

const TICK_RATE: Duration = Duration::from_millis(20);

#[derive(Debug, Clone)]
pub struct Handle {
    input: Arc<Mutex<Input>>,
}
impl Handle {
    pub fn init() -> Self {
        let own = Self {
            input: Arc::new(Mutex::new(Input::new()))
        };

        own.init_ticker();

        own
    }

    pub fn locked_by(&self) -> Option<u64> {
        self.input.lock().unwrap().locked_by()
    }

    pub fn lock(&self, id: u64) -> Result<(), ()> {
        self.input.lock().unwrap().lock(id)
    }

    pub fn release(&self, id: u64) {
        self.input.lock().unwrap().release(id);
    }

    pub fn get_xy(&self) -> [i8; 2] {
        self.input.lock().unwrap().get_xy()
    }

    pub fn set_xy(&self, xy: [i8; 2]) {
        self.input.lock().unwrap().set_xy(xy)
    }

    fn init_ticker(&self) {
        let input = self.input.clone();
        thread::spawn(move || loop {
            let mut input = input.lock().unwrap();

            // back to zero
            if input.locked_by().is_none() {
                input.to_zero();
            }

            drop(input);

            thread::sleep(TICK_RATE);

        });
    }
}

#[derive(Debug, Clone)]
struct Input {
    xy: [i8; 2],
    locked_by: Option<u64>,
}

impl Input {
    fn new() -> Self {
        Self {
            xy: [0, 0],
            locked_by: None,
        }
    }

    fn get_xy(&self) -> [i8; 2] {
        self.xy
    }

    fn set_xy(&mut self, xy: [i8; 2]) {
        self.xy = xy;
    }

    fn locked_by(&self) -> Option<u64> {
        self.locked_by
    }

    fn lock(&mut self, id: u64) -> Result<(), ()> {
        if self.locked_by.is_none() {
            self.locked_by = Some(id);
            return Ok(())
        } else {
            return Err(());
        }
    }

    fn release(&mut self, id: u64) {
        if let Some(locked_id) = self.locked_by {
            if locked_id == id {
                self.locked_by = None;
            }
        }
    }

    fn to_zero(&mut self) {
        self.xy[0] /= 2;
        self.xy[1] /= 2;
    } 
}