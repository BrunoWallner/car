mod compiler;
mod parser;

pub use compiler::compile;
pub use parser::parse;

#[derive(Debug, Clone, PartialEq)]
pub enum OtherKind {
    Information{header: String, children: Vec<String>},
}
impl OtherKind {
    pub fn as_string(&self) -> String {
        match self {
            Self::Information{header, children} => {
                let mut string = String::new();
                string.push_str(&format!("!{}", header.to_uppercase()));
                children.iter().for_each(|i| string.push_str(&format!(" {}", i)));
                string
            }
        }
    }
    pub fn has_closing(&self) -> bool {
        match self {
            Self::Information{..} => false,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Kind {
    // special
    Other(OtherKind),

    Title,
    Meta,
    Html,
    Body,
    Head,
    Div,
    Script,
    Style,
    None,
}
impl Kind {
    pub fn as_string(&self) -> String {
        match self {
            Self::Other(o) => o.as_string(),
            Self::Title => String::from("title"),
            Self::Meta => String::from("meta"),
            Self::Html => String::from("html"),
            Self::Body => String::from("body"),
            Self::Head => String::from("head"),
            Self::Div => String::from("div"),
            Self::Script => String::from("script"),
            Self::Style => String::from("style"),
            Self::None => String::from("NONE"),
        }
    }

    pub fn has_closing(&self) -> bool {
        match self {
            Self::Meta => false,
            Self::Other(o) => o.has_closing(),
            _ => true,
        }
    }

    pub fn all() -> Vec<Self> {
        vec![
            Self::Title,
            Self::Meta,
            Self::Html,
            Self::Body,
            Self::Head,
            Self::Div,
            Self::Script,
            Self::Style,
        ]
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum AttributeKind {
    Id,
    Type,
    Charset,
    Src,
    Lang,
    Style,
}
impl AttributeKind {
    pub fn as_string(&self) -> String {
        match self {
            Self::Id => String::from("id"),
            Self::Type => String::from("type"),
            Self::Charset => String::from("charset"),
            Self::Src => String::from("src"),
            Self::Lang => String::from("lang"),
            Self::Style => String::from("style"),
        }
    }
    pub fn all() -> Vec<Self> {
        vec![
            Self::Id,
            Self::Type,
            Self::Charset,
            Self::Src,
            Self::Lang,
            Self::Style,
        ]
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Attribute {
    pub kind: AttributeKind,
    pub value: String,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Position {
    pub start: usize,
    pub end: usize,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Element {
    pub kind: Kind,
    pub attributes: Vec<Attribute>,
    pub inner: Option<String>,
    pub elements: Vec<Element>,
}