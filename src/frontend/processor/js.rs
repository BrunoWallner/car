use super::slice;

pub fn remove_comments(mut input: String) -> String {
    // delete comments
    let mut pointer: usize = 0;
    loop {
        if let Some(comment_start) = slice(&input, pointer, None).find("/*") {
            let comment_start = comment_start + pointer;

            let comment_end = slice(&input, comment_start, None).find("*/")
                .unwrap_or(input.len() - 1 - comment_start );
            let comment_end = comment_end + comment_start + "*/".len();
            
            input.drain(comment_start..comment_end);

            pointer = comment_start;
        } else {
            break;
        }
    }
    input
}