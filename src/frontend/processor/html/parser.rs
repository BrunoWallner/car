use super::*;
use super::super::slice;

fn remove_comments(mut html: String) -> String {
    let mut pointer: usize = 0;
    loop {
        // comment
        if let Some(comment_start) = slice(&html, pointer, None).find("<!--") {
            let comment_start = comment_start + pointer;

            let comment_end = slice(&html, comment_start, None).find("-->")
                .unwrap_or(html.len() - 1 - comment_start );
            let comment_end = comment_end + comment_start + "-->".len();
            
            html.drain(comment_start..comment_end);

            pointer = comment_start;
        } else {
            break;
        }
    }
    html
}

pub fn parse(html: String) -> Vec<Element> {
    let html = remove_comments(html);
    let mut elements: Vec<Element> = Vec::new();

    let mut pointer: usize = 0;
    // find opening <
    loop {
        if let Some(opening_pos) = slice(&html, pointer, None).find('<') {
            pointer = pointer + opening_pos + 1;
            let further = html.chars().nth(pointer).unwrap_or('/');
            if further != '/' {
                if let Some(ending_pos) = slice(&html, pointer, None).find(">") {
                    /* === Kind determination === */
                    let ending_pos = ending_pos + pointer + 1;
                    let kind_slice = slice(&html, pointer, Some(ending_pos - 1));
                    let mut kind = Kind::None;
                    for k in Kind::all() {
                        // kind must be at start of opening tag -> "== 0"
                        if kind_slice.find(&k.as_string()).unwrap_or(1) == 0 {
                            kind = k;
                            break;
                        }                        
                    }

                    // special Information kind
                    if kind_slice.chars().nth(0) == Some('!') {
                        pointer += 1;
                        let split: Vec<&str> = kind_slice.split(" ").collect();
                        if !split.is_empty() {
                            let header = String::from(&split.get(0).unwrap()[1..]);
                            let children: Vec<String> = split[1..]
                                .iter()
                                .map(|x| String::from(*x))
                                .collect();
                            elements.push(Element {
                                kind: Kind::Other(OtherKind::Information{header, children}),
                                attributes: Vec::with_capacity(0),
                                inner: None,
                                elements: Vec::with_capacity(0),
                            })
                        }
                        
                    }

                    /* === Attribute Determination for normal kinds === */
                    else if kind != Kind::None {
                        let mut attributes: Vec<Attribute> = Vec::new();
                        'finding_attributes: loop {
                            let mut found_kind: bool = false;
                            for attr_kind in AttributeKind::all() {
                                let search = attr_kind.as_string() + "=";
                                if let Some(start) = slice(&html, pointer, Some(ending_pos)).find(&search) {
                                    found_kind = true;
                                    pointer = pointer + start + search.len();
                                    
    
                                    let split = slice(&html, pointer, Some(ending_pos));
                                    let split = split.split("\"").collect::<Vec<&str>>();
    
                                    // get only value eg: '"style.css">' -> style.css
                                    if let Some(attr_value) = split.get(1) {
                                        attributes.push(Attribute {
                                            kind: attr_kind,
                                            value: String::from(*attr_value),
                                        })
                                    }
                                }
                            }
                            if !found_kind {
                                break 'finding_attributes;
                            }
                        }

                        // find end
                        if kind.has_closing() {
                            let closing_str = &format!("</{}>", kind.as_string());
                            if let Some(closing_pos) = slice(&html, pointer, None).find(closing_str) {
    
                                let inner_closing_start = closing_pos + pointer;
    
                                let mut inner = Some(slice(&html, ending_pos, Some(inner_closing_start)));
                                let inner_elements = parse(inner.clone().unwrap());
    
                                if inner.clone().unwrap().is_empty() {
                                    inner = None;
                                }

                                if !inner_elements.is_empty() {
                                    inner = None;
                                }
    
                                elements.push(Element {
                                    kind,
                                    attributes,
                                    elements: inner_elements,
                                    inner,
                                });
    
                                pointer = inner_closing_start;                    
                            }
                        } else {
                            elements.push(Element {
                                kind,
                                attributes,
                                elements: Vec::with_capacity(0),
                                inner: None,
                            })
                        }
    
                    }
                }
            }
        } else {
            break
        }
    }

    elements
}